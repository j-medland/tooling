# Queued Typed-Message Handler

An experimental framework which aims to add Type-Safety into the Queued Message Handling paradigm.

## Motivation
Large applications are built up from small components which must communicate with each other; This communication should happen through carefully built APIs but the necessity to flatten data-types into type-agnostic messages can cause problems as the modular-components change over time.

This framework explores one possible route to overcome this issue via strongly-typed User Event messaging. The concept itself is relatively straight forward but as User Events can require considerable programming effort to create, the main thrust of the framework is to remove the programming overhead.

The framework also represents the many design and style decisions taken to provide a logical and consistent approach to object ownership and lifetime which can trouble applications built from asynchronous components.

## Key Concepts
User Events provide some additional functionality compared to conventional LabVIEW queues. Whilst the creation and sending of a message are similar for User Events and Queues, the handling of messages is quite different. The Queue API provides various dequeue options to get a message from the queue but for User Events, messages are handled using the Event-Structure. This resembles a case structure except that it can be _dynamically_ "subscribed" to User-Event messages. 

As an Event-Structure can be subscribed to many different User Events it is possible to handle different message-queues with a single mechanism with ease. By creating a User-Event for every message, it is possible to enforce a specific data-type for each message and avoid the need to flatten a message to some generic type. 

A useful feature of all LabVIEW-Events is that it is possible to dynamically change if the Event-Structure is listening for that events. This means that it is possible to choose what action will be performed when a particular event occurs or if anything is done at all. This allows for application state to be encapsulated into the event-structure. For example, if the completion of some action is signalled with an event then instead of waiting for that event and then deciding what application state should be transitioned to it is possible to make the receipt of that event cause the state-transition itself.


## Inspiration


## Contributions
Contributions are welcome - simply submit a PR.

The framework is produced with LabVIEW 2015 with tests written with [Caraya](https://github.com/JKISoftware/Caraya)


